package controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.event.ActionEvent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javafx.scene.image.Image;
import model.Account;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import model.Album;
import model.Photo;
import model.Tag;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class AdminController extends BasicController{
    /**
     * This controller implements the functionalites of what an Admin Account is capable of, creating and deleting users.
     *
     * @author Matthew Lee
     * @author Xavier La Rosa
     */
    @FXML TextField username;
    @FXML Button addButton, deleteButton, filterButton, logoutButton;

    @FXML ListView<Account> listView;
    static ObservableList<Account> observableList;
    static ArrayList<Account> accounts = new ArrayList<>();
    // universal variables for button actions
    int index;
    Account selectedAcc;

    /**
     * Initializes the creation of the Admin Screen, fills listview with exisiting users.
     * @throws IOException
     */

    public void start() throws IOException{
        observableList = FXCollections.observableArrayList(accounts);
        listView.setItems(observableList);
        listView.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listView.getSelectionModel().select(0);
        username.setOnKeyTyped((event) ->{
            try{
                System.out.println("detected key press");
                if(username.getText().isEmpty()){
                    accountsFiltered.clear();
                    observableListFiltered.clear();
                    listView.setItems(observableList);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        });
    }

    /**
     * Upon clicking the Add Button, this method instantiates the creation of a new member and adds them to
     * the listview.
     * @param event
     */

    public void handleAddButton(ActionEvent event){
        if(username.getText().isEmpty()){
            System.out.println("Name is empty");
        } else if(accountExists(username.getText())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Admin Error");
            alert.setHeaderText("Cannot add user.");
            alert.setContentText("This user already exists.");
            alert.showAndWait();
        } else{
            System.out.println("Adding user!");
            Account acc = new Account(username.getText());
            for(String str : acc.getCustomTagNames()){
                acc.addCustomTag(new Tag(str));
            }
            Album stockAlbum = new Album("stock");
            File photoFile;
            for (int currentPhoto = 1; currentPhoto <= 5; currentPhoto++) {
                photoFile = new File("src/info/stock/" + currentPhoto + ".JPEG");
                if (photoFile != null) {
                    String name = photoFile.getName();
                    Calendar date = Calendar.getInstance();
                    Image image = new Image(photoFile.toURI().toString());
                    System.out.println("Adding stock images to new account");

                    date.setTimeInMillis(photoFile.lastModified());
                    stockAlbum.addPhoto(new Photo(name, image, date));
                }
            }
            acc.addAlbum(stockAlbum);
            accounts.add(acc);
            observableList.add(acc);
            listView.setItems(observableList);
            listView.getItems();
            updateInfo();
        }
    }

    /**
     * Upon clicking the Delete Button, this method removes a member from the listview and deletes them from the system.
     * @param event
     */

    public void handleDeleteButton(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            System.out.println("Index to delete: " + index + ", acc: " + accounts.get(index));
//        accounts.remove(index);
//        observableList.remove(index);
//        listView.setItems(observableList);

            for (int i = 0; i < accounts.size(); i++) {
                if (accounts.get(i).getUsername().equals(selectedAcc.getUsername())) {
                    accounts.remove(i);
                }
            }
            for (int i = 0; i < observableList.size(); i++) {
                if (observableList.get(i).getUsername().equals(selectedAcc.getUsername())) {
                    observableList.remove(i);
                }
            }
            listView.setItems(observableList);

            updateInfo();
        } else{
            alert.close();
        }
    }

    /**
     * Holds a new list of filtered users.
     */
    static ArrayList<Account> accountsFiltered = new ArrayList<>();
    static ObservableList<Account> observableListFiltered;

    /**
     * Upon clicking the Filter Button, this method generates a new list given specific parameters made by the user.
     * @param event
     */
    public void handleFilterButton(ActionEvent event){
        accountsFiltered.clear();
        if(!username.getText().isEmpty()){
            for(Account acc : accounts){
                if(acc.getUsername().contains(username.getText())){
                    accountsFiltered.add(acc);
                }
            }
            observableListFiltered = FXCollections.observableArrayList(accountsFiltered);
            listView.setItems(observableListFiltered);
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Admin Error");
            alert.setHeaderText("Incomplete input.");
            alert.setContentText("Cannot filter because the textfield is empty.");
            alert.showAndWait();
        }
    }

    /**
     * This method returns true if an account exists in the data file already.
     * @param username
     * @return
     */
    static boolean accountExists(String username){
        for(Account acc : accounts){
            if(acc.getUsername().equalsIgnoreCase(username)){
                System.out.println("User exists");
                return true;
            }
        }
        return false;
    }

    /**
     * This method prints the names of all existing accounts.
     */
    static void printAccounts(){
        System.out.println("Printing accounts");
        for(Account acc : accounts){
            System.out.println(acc.getUsername());
            for(Album al : acc.getAlbums()){
                System.out.println("\talbum: "+al);
                System.out.println("\t\tphotos: "+al.photos);
            }
        }
    }

    /**
     * This method updates the information in the data file in order to be saved after logging out.
     */
    public static void updateInfo(){
        System.out.println("Attempting to update data");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("src/info/info.dat");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(AdminController.accounts);
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("success on update");
        } catch (Exception exception) {
            System.out.println("failed to update");
            exception.printStackTrace();
        }
    }

    /**
     * This method creates an instance of the specific object that is selected within the listview.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listView.getSelectionModel().getSelectedIndex();
            selectedAcc = listView.getSelectionModel().getSelectedItem();
            if(!AdminController.accounts.isEmpty())
            {
                // UI logic
                addButton.setDisable(false);
                deleteButton.setDisable(false);
                filterButton.setDisable(false);

            } else { // no songs existing in list
                addButton.setDisable(true);
                deleteButton.setDisable(true);
                filterButton.setDisable(true);
            }
            if(selectedAcc.getUsername().equals("stock")){
                deleteButton.setDisable(true);
            } else{
                deleteButton.setDisable(false);

            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }
}
