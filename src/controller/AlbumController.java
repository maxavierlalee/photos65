package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This controller implements the Album screen's functionalities that is present upon clicking the openAlbum button
 * inside the MemberScreen
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class AlbumController extends BasicController {
    @FXML Button backButton, logoutButton, searchButton, cancelButton, viewPhotoButton
            ,addButton, moveButton, copyButton, recaptionButton, deleteButton;
    @FXML TextField photoName, caption;
    @FXML Text albumText;

    public static Album album = null;
    @FXML ListView<Photo> listView;
    static ObservableList<Photo> observableList;
    public static int index;
    Photo selectedPh;

    public static Account account;
    @FXML ChoiceBox<Album> albumSelector;
    static ObservableList<Album> observableListAlbums;

    /**
     * This method initializes the Album screen which needs both the specific selected album that was chosen during the
     * previous screen, as well as a list of all the current existing albums. It displays all the photos that are within
     * the chosen album.
     * @param selectedAl
     * @param albums
     * @throws IOException
     */
    public void start(Album selectedAl, List<Album> albums) throws IOException{
        try{
            System.out.println(album+", passAl: "+selectedAl+", passList: "+albums);
            album = selectedAl;
            albumText.setText(album.getName());
            observableList = FXCollections.observableArrayList(album.photos);

            listView.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>() {
            @Override
            public ListCell<Photo> call(ListView<Photo> photoList) {
                return new Thumbnail();
            }
        });
        listView.setItems(observableList);
        listView.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listView.getSelectionModel().select(0);
        photoName.setOnKeyTyped((event) ->{
            try{
                System.out.println("detected key press");
                if(photoName.getText().isEmpty()){
                    photosFiltered.clear();
                    observableListFiltered.clear();
                    listView.setItems(observableList);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        });

        observableListAlbums = FXCollections.observableArrayList(MemberController.acc.getAlbums());
        albumSelector.setItems(observableListAlbums);
        } catch(Exception e){
            e.printStackTrace();
        }
        if(listView.getItems().isEmpty()){
            deleteButton.setDisable(true);
            viewPhotoButton.setDisable(true);
            copyButton.setDisable(true);
            moveButton.setDisable(true);
            searchButton.setDisable(true);
            cancelButton.setDisable(true);
            recaptionButton.setDisable(true);
            albumSelector.setDisable(true);
        } else{
            deleteButton.setDisable(false);
            viewPhotoButton.setDisable(false);
            copyButton.setDisable(false);
            moveButton.setDisable(false);
            searchButton.setDisable(false);
            cancelButton.setDisable(false);
            recaptionButton.setDisable(false);
            albumSelector.setDisable(false);
        }
    }

    /**
     * This method goes back to the previous screen, references the specific member whos is currently logged in.
     * @param event
     */
    public void handleBackButton(ActionEvent event){
        MemberController.acc = account;
        updateScreen("../view/member.fxml", event, new MemberController());
    }
    static ArrayList<Photo> photosFiltered = new ArrayList<>();
    static ObservableList<Photo> observableListFiltered;
    public void handleSearchButton(ActionEvent event){
        photosFiltered.clear();
        if(!photoName.getText().isEmpty()){
            for(Photo ph : album.photos){
                if(ph.getTitle().contains(photoName.getText())){
                    photosFiltered.add(ph);
                }
            }
            observableListFiltered = FXCollections.observableArrayList(photosFiltered);
            listView.setItems(observableListFiltered);
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Album Error");
            alert.setHeaderText("Cannot find photo.");
            alert.setContentText("Photo text is empty!");
            alert.showAndWait();
        }
    }

    /**
     * This method clears and cancels the actions within this screen.
     * @param event
     */
    public void handleCancelButton(ActionEvent event){
        photosFiltered.clear();
        observableListFiltered.clear();
        listView.setItems(observableList);
    }

    /**
     * This method allows the user to add a photo from their desktop and upload it onto the album chosen.
     * @param event
     */
    public void handleAddButton(ActionEvent event){
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose Image");
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files",  "*.JPG", "*.JPEG", "*.jpeg", "*.BMP", "*.gif", "*.GIF", "*.png","*.bmp", "*.jpg", "*.PNG"),
                new FileChooser.ExtensionFilter("JPEG Files", "*.jpg", "*.JPEG", "*.jpeg", "*.JPG"),
                new FileChooser.ExtensionFilter("Bitmap Files",  "*.BMP","*.bmp"),
                new FileChooser.ExtensionFilter("GIF Files", "*.GIF", "*.gif"),
                new FileChooser.ExtensionFilter("PNG Files", "*.png", "*.PNG"));
        File selectedFile = chooser.showOpenDialog(null);
        if (selectedFile != null) {
            Calendar date = Calendar.getInstance();
            date.setTimeInMillis(selectedFile.lastModified());
            Image img = new Image(selectedFile.toURI().toString());
            String title = selectedFile.getName();
            Photo newPhoto = new Photo(title, img, date);
            for(String tagName : account.getCustomTagNames()){
                if(!tagName.equals("Person") || !tagName.equals("Location")){
                    newPhoto.tags.add(new Tag(tagName));
                }
            }
            if (album.photoExists(newPhoto.getTitle())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Photo Error");
                alert.setHeaderText("Could not add photo.");
                alert.setContentText("This photo already exists.");
                alert.showAndWait();
                return;
            } else if(!album.photoExists(newPhoto.getTitle())){
                album.addPhoto(newPhoto);
                observableList.add(newPhoto);
                listView.setItems(observableList);
            }
            AdminController.updateInfo();
        }

        if(listView.getItems().isEmpty()){
            deleteButton.setDisable(true);
            viewPhotoButton.setDisable(true);
            copyButton.setDisable(true);
            moveButton.setDisable(true);
            searchButton.setDisable(true);
            cancelButton.setDisable(true);
            recaptionButton.setDisable(true);
            albumSelector.setDisable(true);
        } else{
            deleteButton.setDisable(false);
            viewPhotoButton.setDisable(false);
            copyButton.setDisable(false);
            moveButton.setDisable(false);
            searchButton.setDisable(false);
            cancelButton.setDisable(false);
            recaptionButton.setDisable(false);
            albumSelector.setDisable(false);
        }
    }

    /**
     * This method will delete the specifc photo chosen by the user from the listview and the data file.
     * @param event
     */
    public void handleDeleteButton(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            for (int i = 0; i < album.photos.size(); i++) {
                if (album.photos.get(i).getTitle().equals(selectedPh.getTitle())) {
                    album.photos.remove(i);
                }
            }
            for (int i = 0; i < observableList.size(); i++) {
                if (observableList.get(i).getTitle().equals(selectedPh.getTitle())) {
                    observableList.remove(i);
                }
            }
            photoName.setText("");
            listView.setItems(observableList);
            AdminController.updateInfo();
        } else {
            alert.close();
        }
        if(listView.getItems().isEmpty()){
            deleteButton.setDisable(true);
            viewPhotoButton.setDisable(true);
            copyButton.setDisable(true);
            moveButton.setDisable(true);
            searchButton.setDisable(true);
            cancelButton.setDisable(true);
            recaptionButton.setDisable(true);
            albumSelector.setDisable(true);
        } else{
            deleteButton.setDisable(false);
            viewPhotoButton.setDisable(false);
            copyButton.setDisable(false);
            moveButton.setDisable(false);
            searchButton.setDisable(false);
            cancelButton.setDisable(false);
            recaptionButton.setDisable(false);
            albumSelector.setDisable(false);
        }
    }

    /**
     * This method moves a selected photo from one album to another.
     * @param event
     */
    public void handleMoveButton(ActionEvent event){
         if(albumSelector.getSelectionModel().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Album Error");
            alert.setHeaderText("Cannot move photo.");
            alert.setContentText("Please select a destination album to move to.");
            alert.showAndWait();
        } else if(albumSelector.getSelectionModel().getSelectedItem().getName().equals(album.getName())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Album Error");
            alert.setHeaderText("Cannot move photo.");
            alert.setContentText("You are not allowed to move photos to the current album you are on!");
            alert.showAndWait();
        } else{
            System.out.println("album selected: "+albumSelector.getSelectionModel().getSelectedItem());
            String albumStr = albumSelector.getSelectionModel().getSelectedItem().getName();

            for(Album al: MemberController.acc.getAlbums()){
                if(al.getName().equals(albumStr)){
                    System.out.println("album match found");
                    if(!al.photoExists(selectedPh.getTitle())){
                        System.out.println("copying photo");
                        al.addPhoto(selectedPh);
                        observableListAlbums = FXCollections.observableArrayList(MemberController.acc.getAlbums());
                        albumSelector.setItems(observableListAlbums);
                        for(int i = 0; i<album.photos.size(); i++){
                            if(album.photos.get(i).getTitle().equals(selectedPh.getTitle())){
                                album.photos.remove(i);
                            }
                        }
                        for(int i = 0; i<observableList.size(); i++){
                            if(observableList.get(i).getTitle().equals(selectedPh.getTitle())){
                                observableList.remove(i);
                            }
                        }
                        photoName.setText("");
                        listView.setItems(observableList);
                        AdminController.updateInfo();
                        AdminController.printAccounts();
                    }
                }
            }
        }
        if(listView.getItems().isEmpty()){
            deleteButton.setDisable(true);
            viewPhotoButton.setDisable(true);
            copyButton.setDisable(true);
            moveButton.setDisable(true);
            searchButton.setDisable(true);
            cancelButton.setDisable(true);
            recaptionButton.setDisable(true);
            albumSelector.setDisable(true);
        } else{
            deleteButton.setDisable(false);
            viewPhotoButton.setDisable(false);
            copyButton.setDisable(false);
            moveButton.setDisable(false);
            searchButton.setDisable(false);
            cancelButton.setDisable(false);
            recaptionButton.setDisable(false);
            albumSelector.setDisable(false);
        }
    }

    /**
     * This method copies the photos from a chosen album, and moves them to another.
     * @param event
     */
    public void handleCopyButton(ActionEvent event){
         if(albumSelector.getSelectionModel().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Album Error");
            alert.setHeaderText("Cannot copy photo.");
            alert.setContentText("Please select a destination album to copy to.");
            alert.showAndWait();
        } else if(albumSelector.getSelectionModel().getSelectedItem().getName().equals(album.getName())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Album Error");
            alert.setHeaderText("Cannot copy photo.");
            alert.setContentText("You are not allowed to copy photos to the current album you are on!");
            alert.showAndWait();
        } else {
            System.out.println("album selected: "+albumSelector.getSelectionModel().getSelectedItem());
            String albumStr = albumSelector.getSelectionModel().getSelectedItem().getName();

            for(Album al: MemberController.acc.getAlbums()){
                if(al.getName().equalsIgnoreCase(albumStr)){
                    System.out.println("album match found");
                    if(!al.photoExists(selectedPh.getTitle())){
                        System.out.println("copying photo");
                        al.addPhoto(selectedPh);
                        observableListAlbums = FXCollections.observableArrayList(MemberController.acc.getAlbums());
                        albumSelector.setItems(observableListAlbums);
                        AdminController.updateInfo();
                        AdminController.printAccounts();
                    }
                }
            }
        }
    }

    /**
     * This mehod allows the user to type a different caption that describes a selectedd photo.
     * @param event
     */
    public void handleRecaptionButton(ActionEvent event){
        String newCatpion = "   " + caption.getText();
        selectedPh.setCaption(newCatpion);
        album.photos.set(index, selectedPh);
        observableList = FXCollections.observableArrayList(album.photos);
        listView.setItems(observableList);
        AdminController.updateInfo();
    }

    /**
     * This method refernces the next screen, PhotoController which requires a selected photo.
     * @param event
     */
    public void handleViewPhotoButton(ActionEvent event) {
        try{
            System.out.println("Clicked");
            String screenPath = "../view/photo.fxml";
            FXMLLoader loader = new FXMLLoader(getClass().getResource(screenPath));
            Parent parent = loader.load();
            PhotoController.account = MemberController.acc;
            PhotoController.album = MemberController.selectedAl;
            PhotoController photoController = loader.getController();
            PhotoController.selectedPh = selectedPh;
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            photoController.start(selectedPh);
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method creates the instance of a specifc item that is selected within the listview.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listView.getSelectionModel().getSelectedIndex();
            selectedPh = listView.getSelectionModel().getSelectedItem();
            if(!album.photos.isEmpty())
            {
                // UI logic
//                deleteButton.setDisable(false);
//                openAlbumButton.setDisable(false);
//                renameButton.setDisable(false);
//                searchPhotosButton.setDisable(false);

            } else { // no songs existing in list
//                deleteButton.setDisable(true);
//                openAlbumButton.setDisable(true);
//                renameButton.setDisable(true);
//                searchPhotosButton.setDisable(true);
            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }
}
