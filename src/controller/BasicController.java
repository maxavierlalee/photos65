package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.event.ActionEvent;

import model.Account;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * This Controller is the super class to most of the other controllers, holding certain methods in which the all use.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class BasicController {
    @FXML Button logoutButton, addButton, deleteButton;

    public void start() throws IOException{

    }

    /**
     * This method logs the user out of their account and saves all of the data that was changed during their session
     * inside the system.
     * @param event
     */
    public void handleLogoutButton(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to logout? (All account data will be saved)", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            try {
                System.out.println("Logout Button Clicked");
                System.out.println("Attempting to update data");
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream("src/info/info.dat");
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(AdminController.accounts);
                    objectOutputStream.close();
                    fileOutputStream.close();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                updateScreen("../view/login.fxml", event, new LoginController());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            AdminController.printAccounts();
        }else {
            alert.close();
        }
    }

    /**
     * This method refreshes the altered screen and updates all of the information given within the screen.
     * @param screenPath
     * @param event
     * @param controller
     */
    public void updateScreen(String screenPath, ActionEvent event, BasicController controller){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource(screenPath));
            Parent parent = loader.load();
            controller = loader.getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            controller.start();
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
