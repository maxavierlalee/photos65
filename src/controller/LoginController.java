package controller;

import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import model.Album;
import model.Account;
import model.Photo;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * This is the first screen that the user sees, allowing them to login as their speoifc user.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class LoginController extends BasicController{
    @FXML Button loginButton;
    @FXML Button exitButton;
    @FXML TextField username;

    private final String infoPath = "src/info/info.dat";

    /**
     * This method initializes the Login screen allowing the user to type their username and log into their account
     *
     * @throws IOException
     */
    public void start() throws IOException{
        File info = new File(infoPath);
        /**
         * If there is no stock account created, it creates it and fills it with the default photos.
         */
        if (!info.isFile() || !info.exists()) {
            System.out.println("File doesnt exist yet");
            try {
                info.createNewFile();
                Album stockAlbum = new Album("stock");
                String stockPhotoPath = "src/info/stock";
                File photoFile;
                for (int currentPhoto = 1; currentPhoto <= 5; currentPhoto++) {
                    photoFile = new File("src/info/stock/" + currentPhoto + ".JPEG");
                    if (photoFile != null) {
                        String name = photoFile.getName();
                        Calendar date = Calendar.getInstance();
                        Image image = new Image(photoFile.toURI().toString());
                        date.setTimeInMillis(photoFile.lastModified());
                        stockAlbum.addPhoto(new Photo(name, image, date));
                    }
                }
                System.out.println("1");
                Account stock = new Account("stock");
                System.out.println("2");
                stock.addAlbum(stockAlbum);
                System.out.println("3");
                AdminController.accounts = new ArrayList<Account>();
                System.out.println("4");
                AdminController.accounts.add(stock);
                System.out.println("5");

                try {
                    System.out.println("inner try");
                    FileOutputStream fileOutputStream = new FileOutputStream(infoPath);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                    objectOutputStream.writeObject(AdminController.accounts);
                    objectOutputStream.close();
                    fileOutputStream.close();
                } catch (Exception exception) {
                    System.out.println("Failed in inner try");
                    exception.printStackTrace();
                }
            } catch (Exception exception) {
                System.out.println("Failed in first try");
                exception.printStackTrace();
            }
        }
    }

    /**
     * This method checks if a user exists and if so, proceeds to the MemberController Screen.
     * @param event
     */
    public void handleLoginButton(ActionEvent event){
        // File exists, proceed to read it
        try {
            FileInputStream fileInputStream = new FileInputStream(infoPath);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            AdminController.accounts = (ArrayList<Account>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();

            Account acc = null;
            for (Account a : AdminController.accounts) {
                if (a.getUsername().equals(username.getText())) {
                    acc = a;
                    System.out.println(acc.getUsername());
                } else{
                    System.out.println("No match: "+a.getUsername());
                }
            }

            if (username.getText().equals("admin") || username.getText().equals("stock") || acc != null || AdminController.accountExists(username.getText())) {
                if (username.getText().equals("admin")) {
                    updateScreen("../view/admin.fxml", event, new AdminController());
                }else if(username.getText().equals("stock")){
                    StockController.account = acc;
                    updateScreen("../view/stock.fxml", event, new StockController());
                }else {
                    MemberController.acc = acc;
                    updateScreen("../view/member.fxml", event, new MemberController());
                }
            } else {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setTitle("Login Error");
                alert.setHeaderText("User not found.");
                alert.setContentText("This user does not exist.");
                alert.showAndWait();
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method exists the application saving all data that was altered during its instance.
     * @param event
     */
    public void handleExitButton(ActionEvent event){

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to quit?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
            System.out.println("Exit Button Clicked");
            AdminController.updateInfo();
            Stage stage = (Stage) exitButton.getScene().getWindow();
            stage.close();
        } else{
            alert.close();
        }
    }
}