package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Account;
import model.Album;
// please

import java.io.IOException;
import java.util.ArrayList;

/**
 * This controller is the screen in which all users are referred to after logging in. It holds the albums that
 * exist and can be altered through the buttons present.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class MemberController extends BasicController{

    @FXML Text userText;
    @FXML Button logoutButton, addAlbumButton, clearButton;
    @FXML Button searchPhotosButton, openAlbumButton, deleteAlbumButton, renameButton;
    @FXML TextField searchAlbums;

    public static Account acc = new Account("default");

    @FXML ListView<Album> listView;
    static ObservableList<Album> observableList;
//    static ArrayList<Album> albums = new ArrayList<>();
    // universal variables for button actions
    int index;
    public static Album selectedAl;

    /**
     * Initializes the MemberController Screen loading their existing albums as well as the default stock album.
     * @throws IOException
     */

    public void start() throws IOException {
        System.out.println(acc.getUsername());
        userText.setText(acc.getUsername());
        observableList = FXCollections.observableArrayList(acc.getAlbums());
        listView.setItems(observableList);
        listView.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listView.getSelectionModel().select(0);
        if(listView.getItems().isEmpty()){
            openAlbumButton.setDisable(true);
            deleteAlbumButton.setDisable(true);
            searchPhotosButton.setDisable(true);
            renameButton.setDisable(true);
            clearButton.setDisable(true);
        } else{
            openAlbumButton.setDisable(false);
            deleteAlbumButton.setDisable(false);
            searchPhotosButton.setDisable(false);
            renameButton.setDisable(false);
            clearButton.setDisable(false);
        }
    }

    /**
     * This method allows the user to create a new empty album given that they provide a name for it.
     * @param event
     */
    public void handleAddAlbumButton(ActionEvent event){
        if(searchAlbums.getText().isEmpty()){
            System.out.println("Name is empty");
        } else if(acc.albumExists(searchAlbums.getText())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Member Error");
            alert.setHeaderText("Cannot add album.");
            alert.setContentText("This album already exists.");
            alert.showAndWait();
        } else{
            System.out.println("Adding album!");
            Album al = new Album(searchAlbums.getText());
            acc.addAlbum(al);
            observableList.add(al);
            listView.setItems(observableList);
            listView.getItems();
            if(listView.getItems().isEmpty()){
                openAlbumButton.setDisable(true);
                deleteAlbumButton.setDisable(true);
                searchPhotosButton.setDisable(true);
                renameButton.setDisable(true);
                clearButton.setDisable(true);
            } else{
                openAlbumButton.setDisable(false);
                deleteAlbumButton.setDisable(false);
                searchPhotosButton.setDisable(false);
                renameButton.setDisable(false);
                clearButton.setDisable(false);
            }
            AdminController.updateInfo();
        }
    }

    /**
     * This method clears the textInput within the screen.
     * @param event
     */
    public void handleClearButton(ActionEvent event){
        searchAlbums.setText("");
    }

    /**
     * This method refers the user to the SearchPhoto screen where they can filter through photos given their
     * specific parameters.
     * @param event
     */
    public void handleSearchPhotosButton(ActionEvent event){
        try{
            String screenPath = "../view/searchPhoto.fxml";
            SearchPhotoController.account = acc;
            updateScreen(screenPath, event, new SearchPhotoController());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method refers the user to the Photo Screen where they can view a specific photo and customize its
     * aspects.
     * @param event
     */
    public void handleOpenAlbumButton(ActionEvent event){
        try{
            String screenPath = "../view/album.fxml";
            AlbumController.account = acc;
            AlbumController.album = selectedAl;
            System.out.println("Selected album to open: "+selectedAl +", index: "+index);
            System.out.println("Account: "+AlbumController.account+", album: "+AlbumController.album);
            FXMLLoader loader = new FXMLLoader(getClass().getResource(screenPath));
            Parent parent = loader.load();
            AlbumController albumController = loader.getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            albumController.start(selectedAl, acc.getAlbums());
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * This method deletes the selected album from the list and removes all data regarding that album from the data
     * file.
     * @param event
     */
    public void handleDeleteAlbumButton(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            System.out.println("Deleting album!");
            acc.deleteAlbum(selectedAl);
            observableList.remove(selectedAl);
            listView.setItems(observableList);
            listView.getItems();
            if(listView.getItems().isEmpty()){
                openAlbumButton.setDisable(true);
                deleteAlbumButton.setDisable(true);
                searchPhotosButton.setDisable(true);
                renameButton.setDisable(true);
                clearButton.setDisable(true);
            } else{
                openAlbumButton.setDisable(false);
                deleteAlbumButton.setDisable(false);
                searchPhotosButton.setDisable(false);
                renameButton.setDisable(false);
                clearButton.setDisable(false);
            }
            AdminController.updateInfo();
        } else{
            alert.close();
        }
    }

    /**
     * This method allows the user to rename a selected album as long as there are no other albums with the same name
     * as the one the user chooses.
     * @param event
     */
    public void handleRenameButton(ActionEvent event){
        System.out.println(searchAlbums.getText());
        if (!acc.albumExists(searchAlbums.getText()) && !searchAlbums.getText().isEmpty()) {
            selectedAl.setName(searchAlbums.getText());
            observableList = FXCollections.observableArrayList(acc.getAlbums());
            listView.setItems(observableList);
            AdminController.updateInfo();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Member Error");
            alert.setHeaderText("Cannot rename album.");
            alert.setContentText("This album already exists or Textfield is empty!");
            alert.showAndWait();
        }
    }

    /**
     * This method creates an instance of the selected item.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listView.getSelectionModel().getSelectedIndex();
            selectedAl = listView.getSelectionModel().getSelectedItem();
            if(!acc.getAlbums().isEmpty())
            {
                // UI logic
                deleteButton.setDisable(false);
                openAlbumButton.setDisable(false);
                renameButton.setDisable(false);
                searchPhotosButton.setDisable(false);

            } else { // no songs existing in list
                deleteButton.setDisable(true);
                openAlbumButton.setDisable(true);
                renameButton.setDisable(true);
                searchPhotosButton.setDisable(true);
            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }
}
