package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.*;


import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * This Controller implements the functionalities that a user would apply towards a specific photo that was selected
 * through the Album Controller.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class PhotoController extends BasicController {
    @FXML Button backButton, logoutButton, leftButton
            , rightButton,editButton, cancelButton, deleteButton;
    @FXML Text photoTitle, date, caption;
    @FXML ImageView imageV;
    public static Account account = null;
    public static Album album = null;
    public static Photo selectedPh;
    SimpleDateFormat dateTimeformat = new SimpleDateFormat("hh:mm a-MM/dd/yyyy ");

    @FXML ListView<Tag> listView;
    static ObservableList<Tag> observableList;
    int index;
    Tag selectedTag;
    @FXML TextField tagType, tagValue;

    /**
     * Initializes the Photo screen given the selected photo chosen from the admin screen.
     * @param selectedPh
     * @throws IOException
     */
    public void start(Photo selectedPh) throws IOException{
        System.out.println("Account passed: "+account);
        System.out.println("\t\tcustom tags: "+account.getCustomTagNames());
        for(Album al : account.getAlbums()){
            System.out.println("\talbum: "+al);
            System.out.println("\t\tphotos: "+al.photos);
        }

        for(String tagName : account.getCustomTagNames()){
            if(!selectedPh.tagExists(tagName)){
                selectedPh.tags.add(new Tag(tagName));
            }
        }

        imageV.setImage(selectedPh.getImg());
        centerImage();
        photoTitle.setText(selectedPh.getTitle());
        date.setText(dateTimeformat.format(selectedPh.getDate().getTime()));
        caption.setText(selectedPh.getCaption());

        System.out.println("printing tags");
        for(Tag t : selectedPh.tags){
            System.out.println(t);
        }
        observableList = FXCollections.observableArrayList(selectedPh.tags);
        listView.setItems(observableList);
        listView.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listView.getSelectionModel().select(0);
    }

    /**
     * This method refers back to the previous album screen in which the current photo selected exists.
     * @param event
     */
    public void handleBackButton(ActionEvent event){
        try {
            MemberController.acc = account;
            MemberController.selectedAl = album;
            System.out.println(album.getName());
            //updateScreen("../view/album.fxml", event, new AlbumController());
            String screenPath = "../view/album.fxml";
            FXMLLoader loader = new FXMLLoader(getClass().getResource(screenPath));
            Parent parent = loader.load();
            AlbumController albumController = loader.getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            albumController.start(album, account.getAlbums());
            stage.setScene(scene);
            stage.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void handleCancelButton(ActionEvent event){

    }
    /**
     * This photo refers to the photo before the current photo given its existence.
     * @param event
     * @throws IOException
     */
    public void handleLeftButton(ActionEvent event) throws IOException{
        start(AlbumController.album.getPhotos().get(AlbumController.index + 1));
        selectedPh = AlbumController.album.getPhotos().get(AlbumController.index + 1);
        AlbumController.index++;
    }
    /**
     * This photo refers to the photo after the current photo given its existence.
     * @param event
     * @throws IOException
     */
    public void handleRightButton(ActionEvent event) throws IOException{
        start(AlbumController.album.getPhotos().get(AlbumController.index - 1));
        selectedPh = AlbumController.album.getPhotos().get(AlbumController.index - 1);
        AlbumController.index--;
    }

    /**
     * This allows the user to edit the name of the photo upon being selected and clicking this button with a filled in
     * new name.
     * @param event
     */
    public void handleEditButton(ActionEvent event) {
        System.out.println("FIRST CLICK");
        if(!tagType.getText().isEmpty() && !tagValue.getText().isEmpty()){
            switch(tagType.getText()){
                case "Person":
                    // check if Person tag still exists, if not then create it again
                    if(!selectedPh.tagExists(tagType.getText())){
                        selectedPh.tags.add(new Tag("Person"));
                    } else {
                        // check if value exists, if not then add it
                        for(int i = 0; i< selectedPh.tags.size(); i++){
                            if(selectedPh.tags.get(i).tagName.equals(tagType.getText()) && !selectedPh.tags.get(i).tagValues.contains(tagValue.getText())){
                                selectedPh.tags.get(i).getTagValues().add(tagValue.getText());
                                observableList = FXCollections.observableArrayList(selectedPh.tags);
                                listView.setItems(observableList);
                            }
                        }
                    }
                    break;
                default:
                    System.out.println("SECOND CLICK");
                    // check if Custom tag still exists, if not then create it again
                    if(!selectedPh.tagExists("Location") && tagType.getText().equals("Location")) {
                        selectedPh.tags.add(new Tag("Location"));
                    } else if(!selectedPh.tagExists(tagType.getText())){
                        selectedPh.tags.add(new Tag(tagType.getText()));
                        account.addCustomTag(new Tag(tagType.getText()));
                    } else {
                        // check if value exists, if not then add it
                        for(int i = 0; i< selectedPh.tags.size(); i++){
                            if(selectedPh.tags.get(i).tagName.equals(tagType.getText()) && !selectedPh.tags.get(i).tagValue.equals(tagValue.getText())){
                                selectedPh.tags.get(i).addValue(tagValue.getText());
                                observableList = FXCollections.observableArrayList(selectedPh.tags);
                                listView.setItems(observableList);
                            }
                        }
                    }
                    break;
            }
            AdminController.updateInfo();
        }
    }

    /**
     * This Button deletes the specific photo chosen from the listview as well as from the data file.
     * @param event
     */
    public void handleDeleteButton(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            System.out.println("CLICKED");
            AlbumController.account = account;
            cellWasSelected();
            System.out.println(selectedTag.getTagName());
            selectedPh.tags.remove(selectedTag);
            account.removeCustomTag(selectedTag);
            observableList = FXCollections.observableArrayList(selectedPh.tags);
            listView.setItems(observableList);
        } else{
            alert.close();
        }
    }

    /**
     * This method creates an instance of the item selected.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listView.getSelectionModel().getSelectedIndex();
            selectedTag = listView.getSelectionModel().getSelectedItem();
            tagType.setText(selectedTag.tagName);
            if(selectedTag.getTagName().equals("Person") || selectedTag.getTagName().equals("Location"))
            {
                // UI logic
                deleteButton.setDisable(true);
            } else {
                deleteButton.setDisable(false);
            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }

    /**
     * This method centers the image that was selected to be viewed.
     */
    public void centerImage() {
        Image img = imageV.getImage();
        if (img != null) {
            double w = 0;
            double h = 0;

            double ratioX = imageV.getFitWidth() / img.getWidth();
            double ratioY = imageV.getFitHeight() / ((Image) img).getHeight();

            double reducCoeff = 0;
            if(ratioX >= ratioY) {
                reducCoeff = ratioY;
            } else {
                reducCoeff = ratioX;
            }

            w = img.getWidth() * reducCoeff;
            h = img.getHeight() * reducCoeff;

            imageV.setX((imageV.getFitWidth() - w) / 2);
            imageV.setY((imageV.getFitHeight() - h) / 2);

        }
    }
}
