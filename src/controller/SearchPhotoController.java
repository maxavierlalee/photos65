package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DateTimeStringConverter;
import model.*;


import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This Controller allowes the user to search and filter specific photos given the user's paremeters.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class SearchPhotoController extends BasicController implements Initializable {
    //    TextField tf = new TextField();
//    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
//tf.setTextFormatter(new TextFormatter<>(new DateTimeStringConverter(format), format.parse("00:00:00")));
    @FXML Button backButton, logoutButton, addButton
            , deleteButton, createButton, searchButton
            , startButton, endButton;
    @FXML TextField tagNameTextField, tagValueTextField, startTimeTextField, endTimeTextField, newAlbumTextField;
    @FXML DatePicker startDatePicker, endDatePicker;
    public static Account account = null;
    public static List<Photo> uniquePhotos = new ArrayList<>();

    @FXML ListView<Tag> listViewTags;
    public static ObservableList<Tag> observableListTags;
    int index;
    Tag selectedTa;

    @FXML ListView<Photo> listViewPhotos;
    public static ObservableList<Photo> observableListPhotos;

    /**
     * Initializes the screen and sets all of the button functionalites as well as displaying the points where a
     * user can apply their desired filters.
     * @throws IOException
     */
    public void start() throws IOException{
        try{
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("hh:mm a");
            startTimeTextField.setTextFormatter(new TextFormatter(new DateTimeStringConverter(dateTimeFormat)));
        }catch(Exception e){
            e.printStackTrace();
        }

        observableListTags = FXCollections.observableArrayList();
        listViewTags.setItems(observableListTags);
        listViewTags.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listViewTags.getSelectionModel().select(0);

        observableListPhotos = FXCollections.observableArrayList();
        listViewPhotos.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>() {
            @Override
            public ListCell<Photo> call(ListView<Photo> photoList) {
                return new Thumbnail();
            }
        });
        listViewPhotos.setItems(observableListPhotos);
        listViewPhotos.getSelectionModel().select(0);

        if(listViewPhotos.getItems().isEmpty()){
            createButton.setDisable(true);
            newAlbumTextField.setDisable(true);
        } else{
            createButton.setDisable(false);
            newAlbumTextField.setDisable(false);
        }
        if(listViewTags.getItems().isEmpty()){
            deleteButton.setDisable(true);
        } else{
            deleteButton.setDisable(false);
        }
    }

    /**
     * Adds a tag to be applied towards the filter
     * @param event
     */
    public void handleAddButton(ActionEvent event){
        if(!tagNameTextField.getText().isEmpty() && !tagValueTextField.getText().isEmpty()){
            if(!tagExists(new Tag(tagNameTextField.getText(), tagValueTextField.getText()))){
                observableListTags.add(new Tag(tagNameTextField.getText(), tagValueTextField.getText()));
                listViewTags.setItems(observableListTags);
            }
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Search Photo Error");
            alert.setHeaderText("Cannot add tag.");
            alert.setContentText("To add a tag, make sure both tag type and tag value are filled out.");
            alert.showAndWait();
        }
        if(listViewPhotos.getItems().isEmpty()){
            createButton.setDisable(true);
            newAlbumTextField.setDisable(true);
        } else{
            createButton.setDisable(false);
            newAlbumTextField.setDisable(false);
        }
        if(listViewTags.getItems().isEmpty()){
            deleteButton.setDisable(true);
        } else{
            deleteButton.setDisable(false);
        }
    }
    public boolean tagExists(Tag tag){
        for(Tag t : listViewTags.getItems()){
            System.out.println("t: "+t+", tag: "+tag);
            if(!tag.tagName.equals("Person") && t.tagName.equals(tag.tagName) && t.tagValue.equals(tag.tagValue)){
                System.out.println("matched returning true");
                return true;
            } else if(tag.tagName.equals("Person") && t.tagName.equals(tag.tagName) && t.tagValues.equals(tag.tagValues)){
                System.out.println("matched returning true");
                return true;
            }
        }
        return false;
    }

    /**
     * Deletes a tag from the search filter.
     * @param event
     */
    public void handleDeleteButton(ActionEvent event){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            observableListTags.remove(selectedTa);
            listViewTags.setItems(observableListTags);
        } else{
            alert.close();
        }
        if(listViewPhotos.getItems().isEmpty()){
            createButton.setDisable(true);
            newAlbumTextField.setDisable(true);
        } else{
            createButton.setDisable(false);
            newAlbumTextField.setDisable(false);
        }
        if(listViewTags.getItems().isEmpty()){
            deleteButton.setDisable(true);
        } else{
            deleteButton.setDisable(false);
        }
    }

    /**
     * Creates a new album based on the filtered list.
     * @param event
     */
    public void handleCreateButton(ActionEvent event){
        if(!account.albumExists(newAlbumTextField.getText()) && !newAlbumTextField.getText().isEmpty() && !listViewPhotos.getItems().isEmpty()){
            account.addAlbum(new Album(newAlbumTextField.getText()));
            for(Album al : account.getAlbums()){
                if(al.getName().equals(newAlbumTextField.getText())){
                    for(Photo ph : listViewPhotos.getItems()){
                        al.addPhoto(ph);
                    }
                }
            }
            AdminController.updateInfo();
        }
        if(listViewPhotos.getItems().isEmpty()){
            createButton.setDisable(true);
            newAlbumTextField.setDisable(true);
        } else{
            createButton.setDisable(false);
            newAlbumTextField.setDisable(false);
        }
        if(listViewTags.getItems().isEmpty()){
            deleteButton.setDisable(true);
        } else{
            deleteButton.setDisable(false);
        }
    }

    /**
     * Created for date formatting
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        startDatePicker.setConverter(
                new StringConverter<>() {
                    final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

                    @Override
                    public String toString(LocalDate date) {
                        return (date != null) ? dateFormatter.format(date) : "";
                    }

                    @Override
                    public LocalDate fromString(String string) {
                        return (string != null && !string.isEmpty())
                                ? LocalDate.parse(string, dateFormatter)
                                : null;
                    }
                });
        endDatePicker.setConverter(
                new StringConverter<>() {
                    final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");

                    @Override
                    public String toString(LocalDate date) {
                        return (date != null) ? dateFormatter.format(date) : "";
                    }

                    @Override
                    public LocalDate fromString(String string) {
                        return (string != null && !string.isEmpty())
                                ? LocalDate.parse(string, dateFormatter)
                                : null;
                    }
                });

    }

    /**
     * Once search is clicked, the listview displays the newly filtered list of photos.
     * @param event
     */
    public void handleSearchButton(ActionEvent event){
        observableListPhotos.clear();
        listViewPhotos.setItems(observableListPhotos);
//        System.out.println("Searching with attributes: ");
//        System.out.println("\tstartTime: "+startTimeTextField.getText());
//        System.out.println("\tstartDate: "+startDatePicker.getValue().toString().replace('-','/'));
//        System.out.println("\tendTime: "+endTimeTextField.getText());
//        System.out.println("\tstartDate: "+endDatePicker.getValue().toString().replace('-','/'));
//
        System.out.println("Creating a list of all unique photos");
        for(Album al : account.getAlbums()){
            for(Photo ph : al.getPhotos()){
                if(!uniquePhotos.contains(ph)){
                    uniquePhotos.add(ph);
                }
            }
        }
        if((startTimeTextField.getText().isEmpty() && startDatePicker.getValue()!=null) || (!startTimeTextField.getText().isEmpty() && startDatePicker.getValue()==null)){
            // if start time empty but date filled
            // or if start time filled but date empty
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Search Photo Error");
            alert.setHeaderText("Incomplete date input.");
            alert.setContentText("Cannot filter, either your time or date for start is empty.");
            alert.showAndWait();
        } else if((endTimeTextField.getText().isEmpty() && endDatePicker.getValue()!=null) || (!endTimeTextField.getText().isEmpty() && endDatePicker.getValue()==null)){
            // if end time empty but date filled
            // or if end time filled but date empty
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Search Photo Error");
            alert.setHeaderText("Incomplete date input.");
            alert.setContentText("Cannot filter, either your time or date for end is empty.");
            alert.showAndWait();
        } else if(startDatePicker.getValue() != null && endDatePicker.getValue() != null && startDatePicker.getValue().isAfter(endDatePicker.getValue())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Search Photo Error");
            alert.setHeaderText("Contradicting date input.");
            alert.setContentText("Start date is not set to a time before end date.");
            alert.showAndWait();
        } else{
            for(Tag t : observableListTags){
                System.out.println("\t"+t);
            }

            // create a list matching all tags
            for(Photo ph : uniquePhotos){
                for(Tag t : listViewTags.getItems()){
                    System.out.println("Comparing t:"+t+", and "+ph.getTags().toString());
                    if(photoHasAMatchingTag(t, ph)){
                        System.out.println("Adding!");
                        observableListPhotos.add(ph);
                        //uniquePhotos.remove(ph);
                        break;
                    }
                }
            }

            // filter list with start dates
            if(startTimeTextField.getText().isEmpty() && startDatePicker.getValue()!=null){
                for(Photo ph : uniquePhotos){
                    ph.dateFromCalendar = new Date();
                    try{
                        Date start=new SimpleDateFormat("dd/MM/yyyy").parse(startDatePicker.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                        System.out.println("Comparing start: "+start +", photo: "+ph.dateFromCalendar);
                        if(ph.dateFromCalendar.after(start) || ph.dateFromCalendar.equals(start)){
                            System.out.println("photo is after or equal start");
                            observableListPhotos.add(ph);
                            //uniquePhotos.remove(ph);
                            break;
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }
            }

            // filter list with start dates
            if(!endTimeTextField.getText().isEmpty() && endDatePicker.getValue()!=null){
                for(Photo ph : uniquePhotos){
                    ph.dateFromCalendar = new Date();
                    try{
                        Date end=new SimpleDateFormat("dd/MM/yyyy").parse(startDatePicker.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                        System.out.println("Comparing start: "+end+", photo: "+ph.dateFromCalendar);
                        if(ph.dateFromCalendar.after(end) || ph.dateFromCalendar.equals(end)){
                            System.out.println("photo is before or equal end");
                            observableListPhotos.add(ph);
                            //uniquePhotos.remove(ph);
                            break;
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                }
            }

            // display the officially filtered list
            listViewPhotos.setItems(observableListPhotos);
            if(listViewPhotos.getItems().isEmpty()){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Search Photo Error");
                alert.setHeaderText("No photo found.");
                alert.setContentText("It seems as though your filter choices did not match any photo.");
                alert.showAndWait();
            }
        }
    }

    /**
     * @param tag
     * @param ph
     * @return true if there is a tag that matches the user's preferred tag
     */
    public boolean photoHasAMatchingTag(Tag tag, Photo ph){
        for(Tag t : ph.getTags()){
            if(t.getTagName().equals("Person")){ // photo tag is person
                if(t.getTagName().equals(tag.getTagName())){ // both tags are person
                    for(String str : t.getTagValues()){
                        System.out.println("Comparing str: "+str+", and: "+tag.getTagValues().get(0));
                        if(str.equals(tag.getTagValues().get(0))){
                            return true;
                        }
                    }
                }
            } else{
                if(t.getTagName().equals(tag.getTagName()) && t.getTagValue().equals(tag.getTagValue())){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Goes back to the album screen.
     * @param event
     */
    public void handleBackButton(ActionEvent event) {
        try{
            MemberController.acc = account;
            updateScreen("../view/member.fxml", event, new MemberController());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Creates an instance of the item selected.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listViewTags.getSelectionModel().getSelectedIndex();
            selectedTa = listViewTags.getSelectionModel().getSelectedItem();
//            if(!acc.getAlbums().isEmpty())
//            {
//                // UI logic
//                deleteButton.setDisable(false);
//                openAlbumButton.setDisable(false);
//                renameButton.setDisable(false);
//                searchPhotosButton.setDisable(false);
//
//            } else { // no songs existing in list
//                deleteButton.setDisable(true);
//                openAlbumButton.setDisable(true);
//                renameButton.setDisable(true);
//                searchPhotosButton.setDisable(true);
//            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }
}
// wtf
