package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.event.ActionEvent;

import java.io.File;
import java.io.IOException;

import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import model.*;

import java.util.ArrayList;
import java.util.Calendar;

import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

/**
 * This Controller is the stock account which is a default account that exists and holds 5-10 default photos
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class StockController extends BasicController{

    public static Account account;
    @FXML Button logoutButton, addButton, deleteButton;
    @FXML ListView<Photo> listView;
    static ObservableList<Photo> observableList;
    int index;
    Photo selectedPh;

    public void start() throws IOException{
        observableList = FXCollections.observableArrayList(account.getAlbums().get(0).getPhotos());
        listView.setCellFactory(new Callback<ListView<Photo>, ListCell<Photo>>() {
            @Override
            public ListCell<Photo> call(ListView<Photo> photoList) {
                return new Thumbnail();
            }
        });
        listView.setItems(observableList);
        listView.getSelectionModel().selectedItemProperty().addListener((listObservable,oldValues,newValues) -> cellWasSelected());
        listView.getSelectionModel().select(0);
    }

    /**
     * This button allowes the stock account to add an additional photo as long at stay within 5-10 photos.
     * @param event
     */
    public void handleAddButton(ActionEvent event){
        Album album = account.getAlbums().get(0);
        if(album.photos.size()>=10){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Photo Error");
            alert.setHeaderText("Could not add photo.");
            alert.setContentText("Stock album cannot have any more than 10 photos.");
            alert.showAndWait();
        } else {
            System.out.println("album 0");
            for (Photo ph : album.photos) {
                System.out.println("album 0: " + ph.getTitle());
            }
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Choose Image");
            chooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Image Files", "*.JPG", "*.JPEG", "*.jpeg", "*.BMP", "*.gif", "*.GIF", "*.png", "*.bmp", "*.jpg", "*.PNG"),
                    new FileChooser.ExtensionFilter("JPEG Files", "*.jpg", "*.JPEG", "*.jpeg", "*.JPG"),
                    new FileChooser.ExtensionFilter("Bitmap Files", "*.BMP", "*.bmp"),
                    new FileChooser.ExtensionFilter("GIF Files", "*.GIF", "*.gif"),
                    new FileChooser.ExtensionFilter("PNG Files", "*.png", "*.PNG"));
            File selectedFile = chooser.showOpenDialog(null);
            if (selectedFile != null) {
                Calendar date = Calendar.getInstance();
                date.setTimeInMillis(selectedFile.lastModified());
                Image img = new Image(selectedFile.toURI().toString());
                String title = selectedFile.getName();
                Photo newPhoto = new Photo(title, img, date);
                for (String tagName : account.getCustomTagNames()) {
                    if (!tagName.equals("Person") || !tagName.equals("Location")) {
                        newPhoto.tags.add(new Tag(tagName));
                    }
                }
                if (album.photoExists(newPhoto.getTitle())) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Photo Error");
                    alert.setHeaderText("Could not add photo.");
                    alert.setContentText("This photo already exists.");
                    alert.showAndWait();
                    return;
                } else if (!album.photoExists(newPhoto.getTitle())) {
                    album.addPhoto(newPhoto);
                    observableList.add(newPhoto);
                    listView.setItems(observableList);
                }
                AdminController.updateInfo();
            }
        }
    }

    /**
     * This button allows the stock account to delete a photo as long as it still has 5-10 photos.
     * @param event
     */
    public void handleDeleteButton(ActionEvent event){
        Album album = account.getAlbums().get(0);
        if(album.photos.size()<=5){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Photo Error");
            alert.setHeaderText("Could not delete photo.");
            alert.setContentText("Stock album cannot have any less than 5 photos.");
            alert.showAndWait();
        } else{
            for(int i = 0; i<album.photos.size(); i++){
                if(album.photos.get(i).getTitle().equals(selectedPh.getTitle())){
                    album.photos.remove(i);
                }
            }
            for(int i = 0; i<observableList.size(); i++){
                if(observableList.get(i).getTitle().equals(selectedPh.getTitle())){
                    observableList.remove(i);
                }
            }
            listView.setItems(observableList);
            AdminController.updateInfo();
        }
    }

    /**
     * This method creates an instance of the item selected.
     */
    public void cellWasSelected(){ // functions provided when a cell is selected in list
        try
        {
            index = listView.getSelectionModel().getSelectedIndex();
            selectedPh = listView.getSelectionModel().getSelectedItem();
            if(!account.getAlbums().get(0).photos.isEmpty())
            {
                // UI logic
//                deleteButton.setDisable(false);
//                openAlbumButton.setDisable(false);
//                renameButton.setDisable(false);
//                searchPhotosButton.setDisable(false);

            } else { // no songs existing in list
//                deleteButton.setDisable(true);
//                openAlbumButton.setDisable(true);
//                renameButton.setDisable(true);
//                searchPhotosButton.setDisable(true);
            }
        }
        catch(Exception e){
            System.out.println("cellWasSelected() -> Caught");
        }
    }
}
