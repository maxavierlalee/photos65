package model;

import controller.AdminController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This method is the central that accounts refer to, holds getters and setters that refer to the specific user.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Account implements Serializable {
    private String username;
    private List<Album> albums = new ArrayList<>();
    private List<String> customTagNames = new ArrayList<>();
    private static final long serialVersionUID = 1784709278836007561L;

    /**
     * All an account needs to be created is a given name
     * @param username
     */
    public Account(String username){
        this.username = username;
        this.customTagNames.add("Person");
        this.customTagNames.add("Location");
    }

    /**
     * Sets the username of the account.
     * @param username
     */
    public void setUsername(String username){
        this.username=username;
    }

    /**
     * return what the user's username is.
     * @return
     */
    public String getUsername(){
        return this.username;
    }

    /**
     * Allows the user to be expressed through a String
     * @return
     */
    public String toString() {
        return this.username;
    }

    /**
     * returns a list of albums that the user holds
     * @return
     */
    public List<Album> getAlbums() {
        return this.albums;
    }

    /**
     * Adds an album object to the list of albums that a user holds.
     * @param album
     */
    public void addAlbum(Album album){
        this.albums.add(album);
        //AdminController.updateInfo();
    }

    /**
     * Deletes a specific album from the list of albums that a user holds.
     * @param album
     */
    public void deleteAlbum(Album album){
        this.albums.remove(album);
    }

    /**
     * Returns true if an ablum already exists within the list of albums that a user holds.
     * @param name
     * @return boolean
     */
    public boolean albumExists(String name){
        for(Album al : albums){
            if(al.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a custom tag to a specific photo if the tag does not already exist.
     * @param tag
     */
    public void addCustomTag(Tag tag){
        if(!customTagNameExists(tag.getTagName())){
            System.out.println("Custom tag does not exist, adding");
            this.customTagNames.add(tag.getTagName());
        } else{
            System.out.println("Custom tag already exists");
        }
    }

    /**
     * Removes a specific tag of a photo.
     * @param tag
     */
    public void removeCustomTag(Tag tag) {
        this.customTagNames.remove(tag.getTagName());
    }

    /**
     * Returns the custom tag that is applied towards a photo
     * @return List
     */
    public List<String> getCustomTagNames(){
        return this.customTagNames;
    }

    /**
     * returns true if a tag already exists and is applied to a specific photo.
     * @param tagName
     * @return
     */
    public boolean customTagNameExists(String tagName){
        for(String name : customTagNames){
            if(tagName.equals(name)){
                System.out.println("Match was found for custom tag");
                return true;
            }
        }
        return false;
    }
}
