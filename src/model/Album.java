package model;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;

/**
 * This is the album object that holds a list of photos and belongs to specifc users.
 *
 * @author Matthew lee
 * @author Xavier La Rosa
 */
public class Album implements Serializable {
    private String name;
    private String caption;
    public List<Photo> photos = new ArrayList<>();
    private String earliestDate;
    private String latestDate;
    private int numOfPhotos=0;

    private static final long serialVersionUID = -4513889939721238518L;
    SimpleDateFormat dateTimeformat = new SimpleDateFormat("hh:mm a-dd/MM/yyyy ");

    public Album (String name) {
        this.name = name;
        System.out.println("photos: "+this.photos);
    }

    public void addPhoto(Photo photo){
        photos.add(photo);
    }

    public void deletePhoto(Photo photo){
        photos.remove(photo);
    }

    /**
     * return the list of photos within this album
     * @return
     */
    public List<Photo> getPhotos(){
        return this.photos;
    }

    void updateCaption(String caption){
        this.caption = caption;
    }

    boolean equals(Album album){
        return album.name.equals(this.name);
    }

    public String getName(){
        return this.name;
    }

    public String toString(){
        numOfPhotos = photos.size();
        if(photos.isEmpty()){
            return name +": "+numOfPhotos+" photo(s)";
        } else{
            try{
                earliestDate = dateTimeformat.format(photos.get(0).getDate().getTime());
                latestDate = dateTimeformat.format(photos.get(photos.size()-1).getDate().getTime());
            } catch(Exception e){
                e.printStackTrace();
            }
            return name + " (" +earliestDate+" to "+latestDate+"): "+numOfPhotos+" photo(s)";
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns true if a photo already exists within a certain album.
     * @param name
     * @return
     */
    public boolean photoExists(String name){
        for(Photo ph : photos){
            System.out.println("Comparing given: "+name+", with list ph: "+ph.getTitle());
            System.out.println("Comparing given size: "+name.length()+", with list ph size: "+ph.getTitle().length());
            if(ph.getTitle().equalsIgnoreCase(name)){
                System.out.println("Exists, returning true");
                return true;
            }
        }
        return false;
    }
}
