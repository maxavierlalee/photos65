package model;

import javafx.scene.image.Image;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Date;

/**
 * Photo object that holds parameters that make a photo unique.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Photo implements Serializable {
    private static final long serialVersionUID = 2574633947395074758L;

    public SerializableImage img;
    public Calendar date;
    public Date dateFromCalendar;
    public ArrayList<Tag> tags;
    public String title;
    public String caption;
    public String timeStr;
    public String dateStr;

    public Photo(){

    }
    SimpleDateFormat sdf= new SimpleDateFormat("hh:mm a-dd/MM/yyyy ");

    /**
     * Constructor for photo that utilizes serializable image so it can be saved and referenced to within the data files.
     * @param title
     * @param image
     * @param date
     */
    public Photo(String title, SerializableImage image, Calendar date) {
        this.title = title;
        this.caption = "";
        this.img = image;
        this.date = date;
        this.tags = new ArrayList<>();
        System.out.println("Adding new tags!");
//        this.tags.add(new Tag("Person"));
//        this.tags.add(new Tag("Location"));
        this.date.set(Calendar.MILLISECOND, 0);
        String[] parts = this.date.toString().split("-");
        timeStr = parts[0];
        this.dateFromCalendar = date.getTime();
        this.dateStr = sdf.format(this.dateFromCalendar);
    }

    /**
     * Constructor for photo using an image object to return certain parameters that are referenced
     * @param name
     * @param image
     * @param date
     */
    public Photo(String name, Image image, Calendar date) {
        this.title = name;
        this.caption = "";
        this.img = new SerializableImage(image);
        this.date = date;
        this.tags = new ArrayList<>();
        System.out.println("Adding new tags!");
//        this.tags.add(new Tag("Person"));
//        this.tags.add(new Tag("Location"));
        this.date.set(Calendar.MILLISECOND, 0);
        String[] parts = this.date.toString().split("-");
        timeStr = parts[0];
//        dateStr = parts[1];
        this.dateFromCalendar = date.getTime();
        this.dateStr = sdf.format(this.dateFromCalendar);
    }

    public String getTitle() {
        return title;
    }

    public String getCaption() {
        return caption;
    }

    public Image getImg() {
        return img.getImage();
    }

    public ArrayList<Tag> getTags() {
        return tags;
    }

    public Calendar getDate() {
        return date;
    }

    public String getTimeStr(){
        return timeStr;
    }

    public String getDateStr(){
        return dateStr;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean equals(Photo otherPhoto) {
        return this.title.equals(otherPhoto.title) && this.img.equals(otherPhoto.img);
    }

    public boolean tagExists(String tagName){
        for(Tag t : tags){
            if(t.getTagName().equals(tagName)){
                return true;
            }
        }
        return false;
    }
    public String toString(){
        return title;
    }
}
