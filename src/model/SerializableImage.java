package model;
import java.io.Serializable;

import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;

/**
 * This Class makes the image object serialzable breaking it down into bytes that allow the to be saved within our
 * data file.
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class SerializableImage implements Serializable {

    private static final long serialVersionUID = -4561912102351823421L;
    public int[][] imagePixels;
    public int imageWidth, imageHeight;

    /**
     * Uses an image obect and rebuilds it by the pixels.
     * @param image
     */
    public SerializableImage(Image image) {
        imageWidth = ((int) image.getWidth());
        imageHeight = ((int) image.getHeight());
        imagePixels = new int[imageWidth][imageHeight];
        PixelReader pixelReader = image.getPixelReader();
        for (int i = 0; i < imageWidth; i++){
            for (int j = 0; j < imageHeight; j++) {
                imagePixels[i][j] = pixelReader.getArgb(i, j);
            }
        }
    }

    /**
     * Converts the serialized image back to an image object.
     * @return
     */
    public Image getImage() {
        WritableImage img = new WritableImage(imageWidth, imageHeight);
        PixelWriter pixelWriter = img.getPixelWriter();
        for (int i = 0; i < imageWidth; i++) {
            for (int j = 0; j < imageHeight; j++) {
                pixelWriter.setArgb(i, j, imagePixels[i][j]);
            }
        }
        return img;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public int[][] getImagePixels() {
        return imagePixels;
    }

    /**
     * Checks whethor or not two images are equal to each other.
     * @param serializableImg
     * @return
     */
    public boolean equals(SerializableImage serializableImg) {
        if (imageWidth != serializableImg.getImageWidth() || imageHeight != serializableImg.getImageHeight()) {
            return false;
        }
        for (int i = 0; i < imageWidth; i++) {
            for (int j = 0; j < imageHeight; j++) {
                if (imagePixels[i][j] != serializableImg.getImagePixels()[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

}

