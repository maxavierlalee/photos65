package model;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

/**
 * This class provides tiny photos that reference the main photos within an albums display
 *
 * @author Matthew Lee
 * @author Xavier La Rosa
 */
public class Thumbnail extends ListCell<Photo> {

    AnchorPane ap = new AnchorPane();
    StackPane sp = new StackPane();
    ImageView img = new ImageView();
    Label label = new Label(), text = new Label(), name = new Label(), title = new Label();

    /**
     * This is the constructor and sets the photos to be a specific size so it is viewable within the album screen.
     */
    public Thumbnail() {
        super();
        name.setFont(Font.font("System", FontWeight.BOLD, 12));
        title.setFont(Font.font(12));

        label.setFont(Font.font("System", FontWeight.BOLD, 12));
        text.setFont(Font.font(12));

        img.setFitWidth(50.0);
        img.setFitHeight(50.0);
        img.setPreserveRatio(true);

        StackPane.setAlignment(img, Pos.CENTER);
        sp.getChildren().add(img);
        sp.setPrefHeight(60.0);
        sp.setPrefWidth(50.0);

        AnchorPane.setLeftAnchor(sp, 0.0);
        AnchorPane.setLeftAnchor(name, 60.0);
        AnchorPane.setTopAnchor(name, 0.0);
        AnchorPane.setLeftAnchor(title, 100.0);
        AnchorPane.setTopAnchor(title, 0.0);
        AnchorPane.setLeftAnchor(label, 60.0);
        AnchorPane.setTopAnchor(label, 25.0);
        AnchorPane.setLeftAnchor(text, 100.0);
        AnchorPane.setTopAnchor(text, 25.0);
        ap.getChildren().addAll(sp, name, title, label, text);
        ap.setPrefHeight(60.0);
        label.setMaxWidth(300.0);
        setGraphic(ap);
    }

    /**
     * refreshes the info of a specific thumbnail.
     * @param photo
     * @param empty
     */
    public void updateItem(Photo photo, boolean empty) {
        super.updateItem(photo, empty);
        setText(null);
        if(photo == null){
            img.setImage(null);
            name.setText("");
            title.setText("");
            label.setText("");
            text.setText("");
        }
        if(photo != null){
            img.setImage(photo.getImg());
            name.setText("Title: ");
            title.setText(photo.getTitle());
            label.setText("Caption: ");
            text.setText(photo.getCaption());
        }
    }

}
