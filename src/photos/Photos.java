package photos;

import controller.AdminController;
import controller.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Photos extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            // display user interface
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../view/login.fxml"));
            Parent root = loader.load();
            LoginController loginController = loader.getController();
            loginController.start();

            // append to scene
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop(){ // save the data before closing
        System.out.println("Saving any existing data to info.dat ...");
        AdminController.updateInfo();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
